import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from "@angular/common/http";
import { DisplayPomComponent } from './display-pom/display-pom.component';

import { AngularD3TreeLibModule } from 'angular-d3-tree';

import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    DisplayPomComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularD3TreeLibModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
