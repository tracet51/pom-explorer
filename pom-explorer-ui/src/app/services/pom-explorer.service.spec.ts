import { TestBed } from '@angular/core/testing';

import { PomExplorerService } from './pom-explorer.service';

describe('PomExplorerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PomExplorerService = TestBed.get(PomExplorerService);
    expect(service).toBeTruthy();
  });
});
