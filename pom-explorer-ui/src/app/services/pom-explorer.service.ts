import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PomExplorerService {
  
  private readonly baseURL = "http://localhost:5153/getPom?location=";

  constructor(private http: HttpClient) { 
  }

  /**getPom
   * name
   */
  public getPom(location: String): Observable<Pom> {
    const url: string = this.baseURL + location;
    return this.http.get<Pom>(url);
  }
}

export interface Pom {
  dependencyName: String;
  dependencies: Array<Pom>
}
