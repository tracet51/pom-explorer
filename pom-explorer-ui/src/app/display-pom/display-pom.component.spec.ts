import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayPomComponent } from './display-pom.component';

describe('DisplayPomComponent', () => {
  let component: DisplayPomComponent;
  let fixture: ComponentFixture<DisplayPomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayPomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayPomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
