import { Component, OnInit, Input, ViewEncapsulation, ViewChild, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { PomExplorerService, Pom } from '../services/pom-explorer.service';
import * as d3 from 'd3';
import { tree, HierarchyNode, treemap, HierarchyPointNode } from 'd3';
import { AngularD3TreeLibService } from 'angular-d3-tree';

@Component({
  selector: 'app-display-pom',
  templateUrl: './display-pom.component.html',
  styleUrls: ['./display-pom.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DisplayPomComponent implements OnInit {

  @ViewChild("chart")
  private chartContainer: ElementRef;

  pomFilter: string;
  pomLocation: string;
  pom: Pom;

  constructor(private pomExplorerService: PomExplorerService) {
  }

  ngOnInit() {
  }

  private isLeaf(): boolean {
    return this.pom.dependencies.length == 0;
  }

  buildTree() {
    // Get the POM
    this.pomExplorerService.getPom(this.pomLocation).subscribe((pom) => {
      if (this.pomFilter != null) {
        pom.dependencies = this.filterPom(this.pomFilter, pom.dependencies);
      }
      this.pom = pom;
      this.constructD3Tree(pom);
    });
  }

  constructD3Tree(pom: Pom) {
    let element = this.chartContainer.nativeElement;
    let canvas = d3.select(element).append("svg")
      .attr("width", "5000px")
      .attr("height", "5000px")
      .append("g")
        .attr("transform", "translate(50,50)");

    let root = d3.hierarchy(this.pom, (d => d.dependencies));
    let tree = d3.tree<Pom>()
      .size([3500, 3500])
      .separation((a, b) => a === b ? 1: 1.5)(root);
    let nodes = tree.descendants();
    let links = tree.links();

    let node = canvas.selectAll(".node")
      .data(nodes)
      .enter()
      .append("g")
        .attr("class", "node")
        .attr("transform", (d => "translate(" + d.y + "," + d.x + ")"))

    node.append("circle")
      .attr("r", 5)
      .attr("fill", "steelblue");

    node.append("text")
      .text(d => d.data.dependencyName.toString());

    canvas.selectAll(".link")
      .data(links)
      .enter()
      .append("line")
      .attr("class", "link")
      .attr("stroke", "#ADADAD")
      .attr("x1", d => d.source.y)
      .attr("y1", d => d.source.x)
      .attr("x2", d => d.target.y)
      .attr("y2", d => d.target.x);
  }

  filterPom(filter: string, poms: Array<Pom>): Array<Pom> {

    let newPom = Array<Pom>();

    for (let index = 0; index < poms.length; index++) {
      const node = poms[index];
      if (node.dependencyName.startsWith(filter)) {
        this.filterPom(filter, node.dependencies);
        newPom.push(node);
      }
      else {
        node.dependencies = []
      }
      
    }
    return newPom;
    
  }

}
