package com.pariveda.pomexplorer;

import java.util.ArrayList;

/**
 * Pom
 */
public class Pom {

    private String dependencyName;
    private ArrayList<Pom> dependencies;
    

    public Pom(String dependencyName, ArrayList<Pom> dependencies) {
        this.dependencyName = dependencyName;
        this.dependencies = dependencies;
    }

    public Pom() {
        this.dependencies = new ArrayList<>();
        this.dependencyName = "Start";
    }

    public String getDependencyName() {
        return this.dependencyName;
    }

    public void setDependencyName(String dependencyName) {
        this.dependencyName = dependencyName;
    }

    public ArrayList<Pom> getDependencies() {
        return this.dependencies;
    }

    public void setDependencies(ArrayList<Pom> dependencies) {
        this.dependencies = dependencies;
    }


}