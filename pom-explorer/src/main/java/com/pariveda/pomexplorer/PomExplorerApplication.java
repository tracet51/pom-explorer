package com.pariveda.pomexplorer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PomExplorerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PomExplorerApplication.class, args);
	}

}
