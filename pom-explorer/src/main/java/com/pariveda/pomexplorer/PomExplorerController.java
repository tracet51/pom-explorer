package com.pariveda.pomexplorer;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Scanner;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * PomController
 */
@CrossOrigin(origins={"http://localhost:4200"})
@RestController
public class PomExplorerController {

    @GetMapping("/getPom")
    public Pom getMavenTree(@RequestParam String location) {

        String directory = "/tmp/";
        String fileName = "dependencyTree.txt";
        String outputLocation = directory +  fileName;

        String cmd = new StringBuilder()
            .append("mvn dependency:tree -f ")
            .append(location)
            .append(" -DoutputFile=")
            .append(outputLocation)
            .toString();

        Pom json = new Pom();
        Runtime run = Runtime.getRuntime();
        Process pr;
        Scanner scanner = null;
        try {
            pr = run.exec(cmd);
            pr.waitFor();

            File file = new File(outputLocation);
            System.out.println(file.getAbsolutePath());
            scanner = new Scanner(file);
            String firstLine = scanner.nextLine();

            Pom pom = new Pom(firstLine, new ArrayList<Pom>());
            json.getDependencies().add(pom);

            ArrayList<String> lines = new ArrayList<>();
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lines.add(line);
            }

            this.fileToJson(lines, json.getDependencies(), new IntHolder(0));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        return json;
    }

    private void fileToJson(ArrayList<String> lines, ArrayList<Pom> json, IntHolder levelsDeep) {
        while (!lines.isEmpty()) {

            String line = lines.get(0);
            String nextLine = lines.get(1);

            int level = StringUtils.countMatches(line, ' ');
            int nextLevel = StringUtils.countMatches(nextLine, ' ');

            if (Math.floorMod(levelsDeep.getNum(), 2) == 0) {
                levelsDeep.setNum(levelsDeep.getNum() - 1);
            }

            line = line.trim();

            if (level >= levelsDeep.getNum()) {

                line = lines.remove(0);
                line = line.replaceAll("[+^:,|\\\\]", "").trim();
                line = line.replace('-', ' ').trim();

                Pom pom = new Pom(line, new ArrayList<Pom>());

                json.add(pom);

                if (nextLevel > level) {
                    this.fileToJson(lines, pom.getDependencies(), new IntHolder(nextLevel));
                }

                if (nextLevel <= level) {
                    this.fileToJson(lines, json, new IntHolder(level));
                }
            }

            if (level < levelsDeep.getNum()) {
                levelsDeep.setNum(levelsDeep.getNum() - 2);
                return;
            }
        }
    }
}

/**
 * IntHolder
 */
class IntHolder {
    
    private int num;
    public IntHolder(int num) {
        this.num = num;
    }


    public int getNum() {
        return this.num;
    }

    public void setNum(int num) {
        this.num = num;
    }

}

/**
 * StringUtils
 */
class StringUtils {

    public static int countMatches(String source, char match) {

        int count = 0;
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);
            if (c == match) count ++;
        }

        return count;
    }
}
